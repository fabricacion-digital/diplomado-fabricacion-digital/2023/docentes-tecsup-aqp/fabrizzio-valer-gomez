# About me

![Fabrizzio Valer](../images/homework01/fotofvg.jpg)

Hi! I am Fabrizzio Valer. I am a teacher 2 years in TECSUP Sede Sur. I work on Mechanical Departament

## Who I am?

My name is Fabrizzio Valer Gómez and I'm 37 years old. I studied mechanical engineering. I also have a master by the Universidad Nacional de San Agustin de Arequipa in Environmetal Sciences. In 2022 I started working in TECSUP , I teach courses in mechanics of materials , fluid mechanics, machine element design, prototyping & mechanism design and vehicle engineering.
![Fabrizzio Valer2](../images/homework01/fotofvg2.jpeg)
<p>&nbsp;</p>
I have two dogs, Zeus & Winnie

![Fabrizzio Valer3](../images/homework01/fotofvg3.jpeg)

## Where I am from?

I'm from Jacobo Hunter in Arequipa, Peru. Arequipa is located in the south of Peru. Jacobo Hunter is one of the 29 districts of the province of Arequipa, located between agricultural areas and the aridity of the San Ignacio and Kasapatac-Ccaccalina hills, where the first pre-Hispanic settlers of Arequipa settled, at an altitude of 2250 meters above sea level and just 15 minutes from the central square of Arequipa.
![Hunter](https://portal.andina.pe/EDPfotografia3/Thumbnail/2023/04/04/000947318W.jpg)
<p>&nbsp;</p>
The district of Jacobo Hunter is one of the few districts that has large areas of countryside whose lands are used for the cultivation of fodder, intended for the feeding of the livestock of the agricultural producer themselves.

![Hunter2](https://1.bp.blogspot.com/-jRYzIDYakjM/XtPMOLhqzII/AAAAAAAACC8/gFMtr-bt498CC6uyL4zj1k85xv4QZzloQCLcBGAsYHQ/s1600/IMG_4360.JPG)
## My Hobby
I have always liked automatas. I worked with wood on some basic models, and now I am exploring 3D printing with PLA filament.

![automata](https://www.keithnewsteadautomata.com/wp-content/uploads/2016/07/IMG_2962-1-e1469964128158.jpg)

## Previous work

I worked in AGM Ingenieros SRL., supervising renewable energy projects, fiber optic installations, hydronic heating systems installation, and training personnel in natural gas and LPG.​ I currently work at Tecsup Sede Sur.

![trabajo2x](https://d2bqn2kyidxvh4.cloudfront.net/wp-content/uploads/2023/03/473712-ANTAPACCAY-258-87e3f6-original-1679925348-808x454.jpg)

