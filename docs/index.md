# Home

## Hello, this is my site.

![](./images/homework01/aqp.jpg)

## Welcome to my Fab Academy site

My name's Fabrizzio Valer Gomez, I'm a teacher in TECSUP Arequipa.

## Presentation

My name is Fabrizzio Valer, I studied Mechanical Engineering at the National University of San Agustín de Arequipa. I have 14 years of experience in the industrial sector, mining, construction, and education.

I am available for any inquiries, and my email address is fvaler@tecsup.edu.pe.
