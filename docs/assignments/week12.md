# 12. Output devices

This week I worked on defining my final project idea and started to getting used to the documentation process.

## Research

"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

> "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."

## Useful links

- [Jekyll](http://jekyll.org)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)

## Code 

The following code turns on all 4 digits at the same time according to the programmed character:

```
void setup() {
  // Activamos los pines
  pinMode(13,OUTPUT);
  pinMode(12,OUTPUT);
  pinMode(14,OUTPUT);
  pinMode(27,OUTPUT);
  pinMode(26,OUTPUT);
  pinMode(25,OUTPUT);
  pinMode(32,OUTPUT);
  pinMode(22,OUTPUT);
}

void loop() {
  // cero
  digitalWrite(13,HIGH);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,LOW);
  digitalWrite(22,HIGH);
  delay(2000);

  //uno
  digitalWrite(13,LOW);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,LOW);
  digitalWrite(26,LOW);
  digitalWrite(25,LOW);
  digitalWrite(32,LOW);
  digitalWrite(22,LOW);
  delay(2000);

  //dos
  digitalWrite(13,HIGH);
  digitalWrite(12,HIGH);
  digitalWrite(14,LOW);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,LOW);
  digitalWrite(32,HIGH);
  digitalWrite(22,HIGH);
  delay(2000);
  
  //tres
  digitalWrite(13,HIGH);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,LOW);
  digitalWrite(25,LOW);
  digitalWrite(32,HIGH);
  digitalWrite(22,LOW);
  delay(2000);

  //cuatro
  digitalWrite(13,LOW);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,LOW);
  digitalWrite(26,LOW);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,HIGH);
  delay(2000);

  //cinco
  digitalWrite(13,HIGH);
  digitalWrite(12,LOW);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,LOW);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,LOW);
  delay(2000);

  //seis
  digitalWrite(13,HIGH);
  digitalWrite(12,LOW);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,HIGH);
  delay(2000);

  //siete
  digitalWrite(13,HIGH);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,LOW);
  digitalWrite(26,LOW);
  digitalWrite(25,LOW);
  digitalWrite(32,LOW);
  digitalWrite(22,LOW);
  delay(2000);

  //ocho
  digitalWrite(13,HIGH);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,HIGH);
  delay(2000);

  //nueve
  digitalWrite(13,HIGH);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,LOW);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,LOW);
  delay(2000);

  // A
  digitalWrite(13,HIGH);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,LOW);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,HIGH);
  delay(2000);

  // C
  digitalWrite(13,HIGH);
  digitalWrite(12,LOW);
  digitalWrite(14,LOW);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,LOW);
  digitalWrite(22,LOW);
  delay(2000);

  // E
  digitalWrite(13,HIGH);
  digitalWrite(12,LOW);
  digitalWrite(14,LOW);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,HIGH);
  delay(2000);

  // F
  digitalWrite(13,HIGH);
  digitalWrite(12,LOW);
  digitalWrite(14,LOW);
  digitalWrite(27,LOW);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,LOW);
  delay(2000);

  // H
  digitalWrite(13,LOW);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,LOW);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,HIGH);
  delay(2000);

  // J
  digitalWrite(13,LOW);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,LOW);
  digitalWrite(32,LOW);
  digitalWrite(22,LOW);
  delay(2000);

  // L
  digitalWrite(13,LOW);
  digitalWrite(12,LOW);
  digitalWrite(14,LOW);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,LOW);
  digitalWrite(22,HIGH);
  delay(2000);

  // P
  digitalWrite(13,HIGH);
  digitalWrite(12,HIGH);
  digitalWrite(14,LOW);
  digitalWrite(27,LOW);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,LOW);
  delay(2000);

  // U
  digitalWrite(13,LOW);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,HIGH);
  digitalWrite(25,HIGH);
  digitalWrite(32,LOW);
  digitalWrite(22,HIGH);
  delay(2000);

  // Y
  digitalWrite(13,LOW);
  digitalWrite(12,HIGH);
  digitalWrite(14,HIGH);
  digitalWrite(27,HIGH);
  digitalWrite(26,LOW);
  digitalWrite(25,HIGH);
  digitalWrite(32,HIGH);
  digitalWrite(22,LOW);
  delay(2000);
  
}
```

## Results
The connection with the ESP32 and the wiring of the 5641AH device is shown
![](../images/28.jpeg)

<iframe width="560" height="315" src="https://www.youtube.com/embed/Tm3XOOdjppU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>


## Final comments
It was very interesting to connect a device that displays characters. However, it can be a little complicated due to my lack of experience with these devices. Now I can display results on a display with 4 characters.
![](../images/29.jpeg)

