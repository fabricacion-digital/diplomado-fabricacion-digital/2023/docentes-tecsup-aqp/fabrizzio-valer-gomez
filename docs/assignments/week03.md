# 3. Computer Aided design

Second week of the Fab Academy where we have one exercise. Model (raster, vector, 2D, 3D, render, animate, simulate, ...) a possible final project, compress our images and videos, and post it on our class page. Here are the Assesment Criteria of this Assignment.
Modelled experimental objects/part of a possible project in 2D and 3D software
Shown how you did it with words/images/screenshots
Included your original design files
It works for me and it helps me to organize the week through the table, so I can be calmer and limit myself to having all the documentation uploaded. I leave Monday and Tuesday to investigate new programs.

## 3D Design

### **Fusion 360**

>![](../images/21.jpg)

## Useful links

- [Jekyll](http://jekyll.org)
- [Google](http://google.com)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)

## Code Example

Use the three backticks to separate code.

```
// the setup function runs once when you press reset or power the board
void setup() {
  // initialize digital pin LED_BUILTIN as an output.
  pinMode(LED_BUILTIN, OUTPUT);
}

// the loop function runs over and over again forever
void loop() {
  digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);                       // wait for a second
}
```

## Gallery

![](../images/sample-photo.jpg)

## Video

### From Vimeo

<iframe src="https://player.vimeo.com/video/10048961" width="640" height="480" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
<p><a href="https://vimeo.com/10048961">Sound Waves</a> from <a href="https://vimeo.com/radarboy">George Gally (Radarboy)</a> on <a href="https://vimeo.com">Vimeo</a>.</p>

### From Youtube

<iframe width="560" height="315" src="https://www.youtube.com/embed/jjNgJFemlC4" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

## 3D Models

Final **Connecting Rod** model created in *Autodesk Fusion 360*

<div class="sketchfab-embed-wrapper"> <iframe title="Connecting Rod" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="640" height="480" src="https://sketchfab.com/models/9af65eaa252d431fa30920d9a6549167/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/connecting-rod-9af65eaa252d431fa30920d9a6549167?utm_medium=embed&utm_campaign=share-popup&utm_content=9af65eaa252d431fa30920d9a6549167" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> Connecting Rod </a> by <a href="https://sketchfab.com/fvaler?utm_medium=embed&utm_campaign=share-popup&utm_content=9af65eaa252d431fa30920d9a6549167" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> fvaler </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=9af65eaa252d431fa30920d9a6549167" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

Final **Crankshaft** model created in *Autodesk Fusion 360*

<div class="sketchfab-embed-wrapper"> <iframe title="Crankshaft" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="640" height="480" src="https://sketchfab.com/models/9be407f052db4f66950d1b5ff4762c45/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/crankshaft-9be407f052db4f66950d1b5ff4762c45?utm_medium=embed&utm_campaign=share-popup&utm_content=9be407f052db4f66950d1b5ff4762c45" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> Crankshaft </a> by <a href="https://sketchfab.com/fvaler?utm_medium=embed&utm_campaign=share-popup&utm_content=9be407f052db4f66950d1b5ff4762c45" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> fvaler </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=9be407f052db4f66950d1b5ff4762c45" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>

Final **Piston** model created in *Autodesk Fusion 360*

<div class="sketchfab-embed-wrapper"> <iframe title="Piston" frameborder="0" allowfullscreen mozallowfullscreen="true" webkitallowfullscreen="true" allow="autoplay; fullscreen; xr-spatial-tracking" xr-spatial-tracking execution-while-out-of-viewport execution-while-not-rendered web-share width="640" height="480" src="https://sketchfab.com/models/bc025c884d82419c9e8c8ae5179484e4/embed"> </iframe> <p style="font-size: 13px; font-weight: normal; margin: 5px; color: #4A4A4A;"> <a href="https://sketchfab.com/3d-models/piston-bc025c884d82419c9e8c8ae5179484e4?utm_medium=embed&utm_campaign=share-popup&utm_content=bc025c884d82419c9e8c8ae5179484e4" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> Piston </a> by <a href="https://sketchfab.com/fvaler?utm_medium=embed&utm_campaign=share-popup&utm_content=bc025c884d82419c9e8c8ae5179484e4" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;"> fvaler </a> on <a href="https://sketchfab.com?utm_medium=embed&utm_campaign=share-popup&utm_content=bc025c884d82419c9e8c8ae5179484e4" target="_blank" rel="nofollow" style="font-weight: bold; color: #1CAAD9;">Sketchfab</a></p></div>


