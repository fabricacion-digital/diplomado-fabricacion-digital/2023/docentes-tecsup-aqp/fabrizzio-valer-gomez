# 1. Principles and practices

My idea for the final project is to build a model for measure the bending deformation.

## **Module of Bending Measure**

### Physical Phenomena

> <iframe width="560" height="315" src="https://www.youtube.com/embed/f08Y39UiC-o" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>

The supports of the model will be printed with a FDM printer and PLA+ filament. All the other pieces will be fabricated with 3mm thick MDF.
<p>&nbsp;</p>
The model measure the deformation of a beam (plate), this is necessary, for example in mechanics of materials course. This module will allow measuring the initial distance and measuring the distance when a controlled force is applied, which should be plotted on a graphic screen or acquired on a laptop for later analysis.
<p>&nbsp;</p>
The beam and rail can be cut with a CNC laser, and distance marks can also be made with the laser engraver.
<p>&nbsp;</p>
This is my project idea, I hope to carry it out during these two weeks. 
<p>&nbsp;</p>

>![module](../images/01.jpeg)


## **Planning**


First week of the Fab Academy where we have the next exercises.

* Principles and Practices and Project Management. Here are the Assesment Criteria of this Assignment.
* Sketched your final project idea/s
* Described what it will do and who will use it
* Made a website and described how you did it
* Introduced yourself
* Documented steps for uploading files to archive
* Pushed to the class archive


>![module](../images/02.jpeg)