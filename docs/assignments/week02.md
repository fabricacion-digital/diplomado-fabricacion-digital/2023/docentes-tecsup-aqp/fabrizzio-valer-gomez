# 2. Project management

## **Sublime Installation**

Follow these steps to install *Sublime Text* Editor:

* The **first** thing I do is download and install [Sublime Text](https://www.sublimetext.com/3)
>![](../images/05.jpg)

## **Git Installation**

Follow these steps to install *Git*:

* The **second** thing I do is download and install [Git](https://git-scm.com/downloads)

>![](../images/03.jpg)

* Git Bash already had it installed. Git Bash is going to be the terminal that I am going to use in any folder.

>![](../images/04.jpg)

## **Create a Gitlab Account**

Follow these steps to create a *Gitlab* account:

* The **third** thing I do is open the link sended by Juan Jose:

>![](../images/07.jpg)

* Then, I create a GitLab account and I accept the project:

>![](../images/06.jpg)

## **Clone Repository Folder**

Follow these steps to clone Repository Folder:

* The **fourth** thing I do is create a folder in my PC called "Repositorio":

>![](../images/08.jpg)

* Then, with left click I select *Open Git Bash here* 

>![](../images/04.jpg)

* This window appears

>![](../images/09.jpg)

```
Laptop@DESKTOP-RSEO95L MINGW64 ~/Documents/TECSUP/Repositorio/fabrizzio-valer-gomez (main)
$
```

* In GitLab copy the URL

>![](../images/10.jpg)

* Type the next command and then, paste the link generated in gitlab (previuos step)

```
Laptop@DESKTOP-RSEO95L MINGW64 ~/Documents/TECSUP/Repositorio/fabrizzio-valer-gomez (main)
$ git clone https:// ...
```

* The repository folder was created in my computer

>![](../images/11.jpg)

## **Generate SSH-Key**

Follow these steps to generate SSH-Key:

* Type the next command

```
Laptop@DESKTOP-RSEO95L MINGW64 ~/Documents/TECSUP/Repositorio/fabrizzio-valer-gomez (main)
$ ssh-keygen -t rsa -b 2048 -C "fvaler@tecsup.edu.pe"
```

>![](../images/12.jpg)


* Type the next command

```
Laptop@DESKTOP-RSEO95L MINGW64 ~/Documents/TECSUP/Repositorio/fabrizzio-valer-gomez (main)
$ cat ~/.ssh/id_rsa.pub
```

>![](../images/13.jpg)

* Paste the SSH-Key in GitLab and commit.

>![](../images/14.jpg)

>![](../images/15.jpg)


* Prompt status

>![](../images/16.jpg)

## **Build My Site**

Follow these steps to build my site

* Open the file "index.md" in "About" folder  with *Sublime Text*

>![](../images/17.jpg)

* Edit the content

>![](../images/18.jpg)

* To publish content in the web

```
$ git status
```

* Then packet the files

```
$ git add .
```

* Then commit the packet

```
$ git commit -m "actualizacion 5"
```

* Then publish the packet

```
$ git push
```

>![](../images/19.jpg)

* Finally, I see the changes in my site

>![](../images/20.jpg)
